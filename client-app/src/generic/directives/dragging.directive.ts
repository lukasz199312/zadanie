import {Directive, ElementRef, HostListener, Inject, Renderer2} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {DraggingNotificationService} from './resizable/dragging-notification.service';

@Directive({
  selector: '[appDraging]'
})
export class DraggingDirective {

  private isDragged = false;
  private pivotPosition: number;

  constructor(public elementRef: ElementRef,
              private renderer: Renderer2,
              private draggingNotificationService: DraggingNotificationService,
              @Inject(DOCUMENT) private document: Document) {

  }

  @HostListener('document:mousemove', ['$event'])
  public onDragLeave(mouseEvent: MouseEvent) {
    if (this.isDragged) {
      this.renderer.setStyle(this.elementRef.nativeElement, 'left', (mouseEvent.clientX - this.pivotPosition) + 'px');
      this.renderer.setStyle(this.elementRef.nativeElement, 'top', mouseEvent.clientY + 'px');
    }
  }

  @HostListener('document:mousedown', ['$event'])
  public onMouseDown(mouseEvent: any): void {
    const clickedInside = this.elementRef.nativeElement.contains(mouseEvent.target);

    if (mouseEvent.target.className !== 'header') {
      return;
    }

    if (clickedInside) {
      this.isDragged = true;
      this.pivotPosition = this.calculatePivotPosition(mouseEvent);

      this.draggingNotificationService.onDragStart();

      this.renderer.setStyle(this.elementRef.nativeElement, 'position', 'absolute');
      this.renderer.setStyle(this.elementRef.nativeElement, 'pointer-events', 'none');
      this.draggingNotificationService.startDragging();
    }
  }

  @HostListener('document:mouseup', ['$event.target'])
  public onMouseUp(targetElement) {
    this.isDragged = false;
    this.renderer.setStyle(this.elementRef.nativeElement, 'pointer-events', 'all');
    this.draggingNotificationService.endDragging();
  }

  private calculatePivotPosition(mouseEvent: any) {
    return mouseEvent.clientX - mouseEvent.target.getBoundingClientRect().x;
  }
}
