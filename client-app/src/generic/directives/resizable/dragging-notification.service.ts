import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class DraggingNotificationService {

  private dragStart$: Subject<void> = new Subject<void>();
  private dragEnd$: Subject<void> = new Subject<void>();

  onDragStart(): Observable<void> {
    return this.dragStart$.asObservable();
  }

  startDragging(): void {
    this.dragStart$.next();
  }

  onDragEnd(): Observable<void> {
    return this.dragEnd$.asObservable();
  }

  endDragging(): void {
    return this.dragEnd$.next();
  }

}
