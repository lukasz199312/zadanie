import {AfterViewInit, ChangeDetectorRef, Directive, HostBinding, HostListener, OnDestroy} from '@angular/core';
import {DraggingNotificationService} from './dragging-notification.service';
import {Subject} from 'rxjs';

@Directive({
  selector: '[appResizable]'
})
export class ResizableDirective implements AfterViewInit, OnDestroy {
  @HostBinding('style.width')
  width: string;

  private onDestroy$: Subject<void> = new Subject<void>();
  private isElementDragging: boolean = false;

  constructor(private draggingNotificationService: DraggingNotificationService,
              private changeDetectorRef: ChangeDetectorRef) {

  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(mouseEvent: MouseEvent) {
    if (this.isElementDragging) {
      return;
    }

    if (mouseEvent.x < 400) {
      this.width = '270px';
    } else {
      this.width = '0px';
    }
  }


}
