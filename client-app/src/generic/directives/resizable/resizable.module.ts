import {NgModule} from '@angular/core';
import {ResizableDirective} from './resizable.directive';
import {DraggingNotificationService} from './dragging-notification.service';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [CommonModule],
  declarations: [ResizableDirective],
  exports: [
    ResizableDirective
  ],
  providers: [DraggingNotificationService]
})
export class ResizableModule {
}
