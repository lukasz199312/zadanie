import {NgModule} from '@angular/core';
import {LayerPanelComponent} from './ui/layer-panel.component';
import {CommonModule} from '@angular/common';
import {DraggingDirective} from '../directives/dragging.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [LayerPanelComponent, DraggingDirective],
  exports: [LayerPanelComponent]
})
export class LayerPanelModule {}
