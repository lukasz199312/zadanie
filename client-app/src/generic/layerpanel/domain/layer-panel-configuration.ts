export class LayerPanelConfiguration {

  private title: string;
  private layers: Array<string> = [];

  withTitle(title: string): LayerPanelConfiguration {
    this.title = title;
    return this;
  }

  getTitle(): string {
    return this.title;
  }

  getLayers(): Array<string> {
    return this.layers;
  }

  addLayer(name: string): LayerPanelConfiguration {
    this.layers.push(name);
    return this;
  }
}
