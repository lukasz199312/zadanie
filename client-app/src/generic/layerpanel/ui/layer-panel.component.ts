import {Component, ElementRef, Input} from '@angular/core';
import {LayerPanelConfiguration} from '../domain/layer-panel-configuration';

@Component({
  selector: 'app-layer-panel',
  templateUrl: './layer-panel.component.html',
  styleUrls: ['./layer-panel.component.sass']
})
export class LayerPanelComponent {

  private counter: number = 1;

  @Input()
  configuration: LayerPanelConfiguration;
  inDockedMode: boolean = false;

  constructor(public elementRef: ElementRef) {

  }

  addLayer() {
    this.configuration.addLayer('Nowa Warstwa ' + this.counter);
    this.counter++;
  }

  switchToDockerMode(): void {
    this.inDockedMode = true;
  }
}
