import {NgModule} from '@angular/core';
import {OpenLayersComponent} from './open.layers.component';

@NgModule({
  declarations: [OpenLayersComponent],
  exports: [OpenLayersComponent]
})
export class OpenLayersModule {
}
