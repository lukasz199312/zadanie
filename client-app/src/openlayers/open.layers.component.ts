import {Component, AfterViewInit, OnDestroy, ChangeDetectorRef, ElementRef} from '@angular/core';
import {DraggingNotificationService} from '../generic/directives/resizable/dragging-notification.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

declare let OpenLayers: any;

@Component({
  selector: 'app-open-layers',
  templateUrl: './open.layers.component.html',
  styleUrls: ['./open.layers.component.sass']
})
export class OpenLayersComponent implements AfterViewInit, OnDestroy {

  pointerEventDisabled = false;

  private onDestroy$: Subject<void> = new Subject<void>();

  constructor(private draggingNotificationService: DraggingNotificationService,
              private changeDetectorRef: ChangeDetectorRef) {
    this.subscribeDraggingStart();
    this.subscribeDraggingEnd();
  }

  ngAfterViewInit() {
    var map = new OpenLayers.Map('map', {
      controls: [
        new OpenLayers.Control.Navigation(),
        new OpenLayers.Control.ArgParser(),
        new OpenLayers.Control.Attribution()
      ]
    });

    var osm = new OpenLayers.Layer.OSM();
    map.addLayer(osm);

    map.zoomToMaxExtent();
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  private subscribeDraggingStart(): void {
    this.draggingNotificationService.onDragStart()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.pointerEventDisabled = true;
        this.changeDetectorRef.markForCheck();
      });
  }

  private subscribeDraggingEnd(): void {
    this.draggingNotificationService.onDragEnd()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => this.pointerEventDisabled = false);
  }

}
