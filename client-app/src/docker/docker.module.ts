import {NgModule} from '@angular/core';
import {DockerZoneComponent} from './ui/docker.zone.component';

@NgModule({
  declarations: [DockerZoneComponent],
  exports: [DockerZoneComponent]
})
export class DockerModule {

}
