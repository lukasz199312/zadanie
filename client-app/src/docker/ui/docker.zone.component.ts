import {
  AfterViewInit,
  ChangeDetectorRef,
  Component, ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  OnDestroy,
  Output
} from '@angular/core';
import {Subject} from 'rxjs';
import {DraggingNotificationService} from '../../generic/directives/resizable/dragging-notification.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-docker-zone',
  templateUrl: './docker.zone.component.html',
  styleUrls: ['./docker-zone.component.sass']
})
export class DockerZoneComponent implements AfterViewInit, OnDestroy {
  @HostBinding('style.width')
  width: string;

  @Output()
  elementDocked: EventEmitter<void> = new EventEmitter<void>();

  isExpanded: boolean = false;
  mouseOverDocker: boolean = false;

  private onDestroy$: Subject<void> = new Subject<void>();
  private isElementDragging: boolean = false;
  private isElementDocked: boolean = false;

  constructor(private draggingNotificationService: DraggingNotificationService,
              private changeDetectorRef: ChangeDetectorRef,
              public elementRef: ElementRef) {
    this.subscribeDraggingStart();
    this.subscribeDraggingEnd();
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(mouseEvent: MouseEvent) {
    if (!this.isElementDragging || this.isElementDocked) {
      return;
    }

    if (mouseEvent.x < 400) {
      this.width = '300px';
      this.isExpanded = true;
    } else {
      this.width = '0px';
      this.isExpanded = false;
    }
  }

  @HostListener('mouseenter', ['$event'])
  onMouseEnter(targetElement) {
    if (this.isExpanded) {
      this.mouseOverDocker = true;
    }
  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave(targetElement) {
    this.mouseOverDocker = false;
  }

  private subscribeDraggingStart(): void {
    this.draggingNotificationService.onDragStart()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.isElementDragging = true;
        this.changeDetectorRef.markForCheck();
      });
  }

  private subscribeDraggingEnd(): void {
    this.draggingNotificationService.onDragEnd()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        if (this.mouseOverDocker) {
          this.elementDocked.next();
          this.isElementDocked = true;
          this.isExpanded = false;
          this.onDestroy$.next();
          this.onDestroy$.complete();
          return;
        }

        this.isElementDragging = false;
        this.width = '0px';
      });
  }
}
