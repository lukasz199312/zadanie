import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {OpenLayersModule} from '../openlayers/open.layers.module';
import {DockerModule} from '../docker/docker.module';
import {ResizableModule} from '../generic/directives/resizable/resizable.module';
import {LayerPanelModule} from '../generic/layerpanel/layer-panel.module';

@NgModule({
  imports: [
    BrowserModule,
    ResizableModule,
    OpenLayersModule,
    DockerModule,
    LayerPanelModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
