import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {LayerPanelConfiguration} from '../generic/layerpanel/domain/layer-panel-configuration';
import {LayerPanelComponent} from '../generic/layerpanel/ui/layer-panel.component';
import {DockerZoneComponent} from '../docker/ui/docker.zone.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  @ViewChild(LayerPanelComponent)
  layerPanelComponent: LayerPanelComponent;

  @ViewChild(DockerZoneComponent)
  dockerZoneComponent: DockerZoneComponent;

  layerPanelConfiguration: LayerPanelConfiguration = new LayerPanelConfiguration();

  constructor(private renderer2: Renderer2) {
  }

  ngOnInit() {
    this.layerPanelConfiguration
      .withTitle('Warstwy')
      .addLayer('Warstwa 1')
      .addLayer('Warstwa 2')
      .addLayer('Warstwa 3');
  }


  onElementDocked(): void {
    this.renderer2.appendChild(this.dockerZoneComponent.elementRef.nativeElement, this.layerPanelComponent.elementRef.nativeElement);
    this.layerPanelComponent.switchToDockerMode();
  }
}
